import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class AddMessageArgs {

  @Field()
  text: string;

  @Field()
  priority: string;
}
