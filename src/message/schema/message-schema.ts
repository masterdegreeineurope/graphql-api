import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class MessageSchema {
  @Field((type) => Int )
  id: number;

  @Field()
  text: string;

  @Field()
  priority: string;
}
