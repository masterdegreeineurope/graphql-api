import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { MessageSchema } from "./schema/message-schema";
import { MessageService } from "./message.service";
import { AddMessageArgs } from "./args/add-message.args";


@Resolver(of => MessageSchema)
export class MessageResolver {
  constructor(private readonly messageService: MessageService) {}

  @Query(returns => [MessageSchema], { name: "message" })
  getAllMessage(){
    return this.messageService.getMessage();
  }
0
  @Mutation(returns => String, { name:"addMessages"})
  async addMessage(@Args("adMessageArgs") addMessageArgs: AddMessageArgs){
    return this.messageService.addMessage(addMessageArgs)
  }
}
